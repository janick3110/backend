package assets

import (
	"encoding/json"
	"fmt"
	"github.com/go-pg/pg/v10"
	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
	"go/src/cors"
	"net/http"
)

type Building struct {
	Id      uuid.UUID `pg:",pk,notnull,type:uuid,default:uuid_generate_v4()" json:"id"`
	Name    string    `pg:",notnull" json:"name"`
	Address string    `json:"address"`
}

func HandleBuilding(db *pg.DB) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		muxVars := mux.Vars(r)
		cors.AddHeaders(w, r)
		w.Header().Set("Content-Type", "application/json")
		if r.Method == "GET" {
			building := &Building{Id: uuid.FromStringOrNil(muxVars["id"])}
			err := db.Model(building).WherePK().Select()
			if err != nil {
				panic(err)
			}
			b, err := json.Marshal(&building)
			fmt.Fprintf(w, string(b))
		} else {
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	})
}

func HandleBuildings(db *pg.DB) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cors.AddHeaders(w, r)
		w.Header().Set("Content-Type", "application/json")
		if r.Method == "GET" {
			var buildings []Building
			err := db.Model(&buildings).Select()
			if err != nil {
				panic(err)
			}
			b, err := json.Marshal(&buildings)
			fmt.Fprintf(w, string(b))
		} else {
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	})
}
