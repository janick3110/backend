module go

go 1.16

require (
	cloud.google.com/go v0.100.2 // indirect
	github.com/go-pg/pg/v10 v10.10.6 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
)
